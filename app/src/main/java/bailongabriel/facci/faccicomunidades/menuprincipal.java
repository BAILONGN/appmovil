package bailongabriel.facci.faccicomunidades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class menuprincipal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menuprincipal);
    }
    public void talleres(View view) {
        Intent talleres = new Intent(this, menuprincipal.class);
        startActivity(talleres);
    }
    public void eventos(View view) {
        Intent eventos = new Intent(this, menuprincipal.class);
        startActivity(eventos);
    }
    public void beneficios(View view) {
        Intent beneficios = new Intent(this, menuprincipal.class);
        startActivity(beneficios);
    }
    public void concursos(View view) {
        Intent concursos = new Intent(this, menuprincipal.class);
        startActivity(concursos);
    }
}


